(function(){
  $(window).ready(function(){
    // @prettify
    prettyPrint();

    // 外部リンクの設定
    var outerLinks = $('a[href^="http://"], a[href^="https://"]');
    if(outerLinks.length){
      var outerLink = new OuterLink(outerLinks);
    }

    // bootstrap の affix 対応
    var styleElm = document.createElement('style');
    styleElm.type = 'text/css';
    styleElm.media = 'screen';
    $('head')[0].appendChild(styleElm);
    $(styleElm).html('#sidebar .affix{width:' + $('#sidebar').width() + 'px}');
    $('#sidebar .nav').affix({offset: {top: ($('header').height() + 40)}});
    $(window).on('resize', function(){
      $(styleElm).html('#sidebar .affix{width:' + $('#sidebar').width() + 'px}');
      $('#sidebar .nav').affix({offset: {top: ($('header').height() + 40)}});
    })
  
    // デモ
    var css3animate_timingFunction = new Css3animate_demo($('#timingFunction'));
    var css3animate_demo1 = new Css3animate_demo($('#example_1'));
    var css3animate_demo2 = new Css3animate_demo($('#example_2'));
    var css3animate_demo3 = new Css3animate_demo($('#example_3'));
  });

  /**
   *  外部リンクの設定
   */
  var OuterLink = function(outerLinks){
    var linkIcon = document.createElement('img');
    $(linkIcon).attr({
      'src': '/img/link.gif',
      'class': 'outerLinkIcon'
    });
    outerLinks.append(linkIcon);
    outerLinks.on('click',
      function(){
        window.open($(this).attr('href'));
        return false;
      }
    )    
  }


  /**
   *  デモ
   */
  var Css3animate_demo = function(elm){
    var that = this;
    this.elm = elm;
    this.isStart = false;
    elm.find('.demo-start').on('click', function(){
      switch(that.elm.attr('id')){
        case 'timingFunction':
          that.start_timingFunction();
          break;
        case 'example_1':
          that.start_example_1();
          break;
        case 'example_2':
          that.start_example_2();
          break;
        case 'example_3':
          that.start_example_3();
          break;
      }
    });
    elm.find('.demo-stop').on('click', function(){
      that.stop(that.elm); 
    });
  }
  Css3animate_demo.prototype = {
    start_timingFunction: function(){
      if($('#timingFunction .demo-area .axis-y').css3animate('status') == 'paused'){
        $('#timingFunction .demo-area .axis-y').css3animate('start');
        $('#timingFunction .demo-area .axis-x').css3animate('start');
        return;
      }

      jQuery('#timingFunction input[name=timing]').on('change',function(){
        jQuery('#timingFunction .axis-y').css3animate (
          {top: '0'},
          {top: '100%'},
          {duration: 800, timingFunction: $(this).val(), iterationCount: 'infinite', direction: 'alternate'}
        );
      });
      jQuery('#timingFunction .axis-y').css3animate (
        {top: '0'},
        {top: '100%'},
        {duration: 800, timingFunction: $('#timingFunction input[name=timing]:checked').val(), iterationCount: 'infinite', direction: 'alternate'}
      );
      jQuery('#timingFunction .axis-x').css3animate (
        {left: '0'},
        {left: '100%'},
        {duration: 4000, iterationCount: 'infinite', timingFunction: 'linear'}
      );
    },
    start_example_1: function(){
      if($('#example_1 .sprite').css3animate('status') == 'paused'){
        $('#example_1 .sprite').css3animate('start');
        return;
      }
      
      jQuery('#example_1 .sprite').html('');
      jQuery('#example_1 .sprite').css3animate (
        {width: '0', height: '0'},
        {width: '100%', height: '200px'},
        {duration: 1000, iterationCount: '1', direction: 'alternate'},
        function(event){
          jQuery(event.target).css({color: '#ffffff', fontSize: '17px', lineHeight: '200px', textAlign: 'center'});
          jQuery(event.target).html('すばらしいMoveをみせた');
        }
      );
    },
    start_example_2: function(){
      if($('#example_2 .sprite').css3animate('status') == 'paused'){
        $('#example_2 .sprite').css3animate('start');
        return;
      }

      var isMove = false;
      var maxAxisX = jQuery('#example_2 .demo-area').width() - 10;
      var maxAxisY = jQuery('#example_2 .demo-area').height() - 10;
      jQuery('#example_2 .demo-area').on('mousemove',
        function(e){
          if(isMove == false){
            setTimeout(function(){
              jQuery('#example_2 .sprite').css3animate (
                {top : ((e.offsetY < maxAxisY)?e.offsetY:maxAxisY) + 'px', left : ((e.offsetX < maxAxisX)?e.offsetX:maxAxisX) + 'px'},
                {duration : 1, timingFunction: 'linear'},
                function(){
                  isMove = false;
                }
              );
            }, 13);
          }
          isMove = true;
        }
      );
    },
    start_example_3: function(){
      if($('#example_3 .sprite').css3animate('status') == 'paused'){
        $('#example_3 .sprite').css3animate('start');
        $('#example_3 .axis').css3animate('start');
        return;
      }
      if(this.isStart == true){
        return;
      }
      this.isStart = true;

      jQuery('#example_3 .demo-area').on('click', '.axis',
        function(e){
          e.stopPropagation();
        }
      );

      jQuery('#example_3 .demo-area').on('click',
        function(e){
          var maxAxisY = jQuery('#example_3 .demo-area').height() - 10;
          var maxAxisX = jQuery('#example_3 .demo-area').width() - 10;
          var axisXelm = document.createElement('div');
          jQuery(axisXelm).attr('class', 'axis');
          jQuery(axisXelm).css({
            top: ((e.offsetY < maxAxisY)?e.offsetY:maxAxisY) + 'px'
          });
          var sprite = document.createElement('div');
          jQuery(sprite).attr('class', 'sprite');
          jQuery(axisXelm).append(sprite);
          jQuery('#example_3 .demo-area').append(axisXelm);

          jQuery(sprite).css3animate(
            {left : '0'},
            {left : maxAxisX + 'px'},
            {duration : 1000,  timingFunction: 'linear', iterationCount: 'infinite', direction: 'alternate', delay: (e.offsetX / maxAxisX) * -1000 }
          );
          jQuery(axisXelm).data('height', maxAxisY - e.offsetY);
          jQuery(axisXelm).css3animate(
            {top : maxAxisY + 'px'},
            {duration : Math.sqrt(jQuery(axisXelm).data('height')) * 30,  timingFunction: 'sine-in', iterationCount: 1, direction: 'alternate'},
            function(e){
              jumpHeight = (jQuery(e.target).data('height') * 0.8);
              if(jumpHeight <= 10){
                jQuery(e.target).css3animate(
                  {top : maxAxisY + 'px', opacity : 1},
                  {top : maxAxisY + 'px', opacity : 0},
                  {duration : 1000},
                  function(e){
                    $(e.target).remove();
                  }
                );
                return;
              }
              jQuery(e.target).data('height', jumpHeight);
              jQuery(e.target).css3animate(
                {top : maxAxisY + 'px'},
                {top : maxAxisY - jumpHeight + 'px'},
                {duration : Math.sqrt(jumpHeight) * 30, timingFunction: 'sine-out', iterationCount: '2', direction: 'alternate'},
                arguments.callee
              );
            }
          );
        }
      );
    },

    stop: function(elm){
      switch(elm.attr('id')){
        case 'timingFunction':
          $('#timingFunction .axis-y').css3animate('stop');
          $('#timingFunction .axis-x').css3animate('stop');
          break;
        case 'example_1':
          $('#example_1 .sprite').css3animate ('stop');
          break;
        case 'example_2':
          $('#example_2 .sprite').css3animate ('stop');
          break;
        case 'example_3':
          $('#example_3 .sprite').css3animate('stop');
          $('#example_3 .axis').css3animate('stop');
          break;
      }
    }
  }
  
  

  /**
   * amazon アソシエイト
   */
  amazon_ad_tag = "logicle01-22";
  amazon_ad_width = "160";
  amazon_ad_height = "600";
  amazon_ad_link_target = "new";
  amazon_ad_border = "hide";


  /*
   * Google Analytics
   */
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41208403-1', 'oai.so');
  ga('send', 'pageview');

})();

